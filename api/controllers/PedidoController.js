/**
 * PedidoController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  createPedido: async (req, res) => {
    let data = req.allParams();

    try {
      const nuevoPedido = await sails.getDatastore().transaction(async (db, _proceed) => {

        const pedido = await Pedido.create({
          nit: data.nit,
          fecha: new Date(),
        }).usingConnection(db).fetch();

        const { id } = pedido;
        if (!id) throw Error('Es necesario crear un pedido para poder agregar el detalle');

        let itemsArrayCreated = []
        for (const iterator of data.detallePedido) {
          const { producto, cantidad, valorProducto, valorLinea } = iterator;

          const detallePedido = await Detalle_pedido.create({
            pedido: id,
            producto: producto,
            cantidad: cantidad,
            valor_producto: valorProducto,
            valor_linea: valorLinea,
            iva: 0,
          })
            .usingConnection(db)
            .fetch();

          itemsArrayCreated.push({
            detallePedido,
          });
        }
        return _proceed(null, { pedido: pedido, itemsArrayCreated });
      });
      res.ok(nuevoPedido);
    } catch (error) {
      return res.status(400).send(error.message);
    }
  },
  getPedido: async (req, res) => {
    let SQL = `SELECT * FROM view_detalle_pedido;`
    let counterSQL = await sails.sendNativeQuery(SQL);
    return res.ok(counterSQL.rows);
  }
};

