/**
 * Detalle_pedido.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    pedido: {
      model: 'pedido'
    },
    producto: {
      model: 'productos'
    },
    cantidad: {
      type: 'number',
      required: true,
    },
    valor_producto: {
      type: 'number',
      required: true,
    },
    valor_linea: {
      type: 'number',
      required: true,
    },
    iva: {
      type: 'number',
      defaultsTo: 0,
      allowNull: true,
      columnType: 'citext',
    },
  },
};

