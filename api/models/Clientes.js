/**
 * Clientes.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    id: {
      type: 'string',
      columnName: 'nit',
      required: true,
      columnType: 'citext',
    },
    nombre: {
      type: 'string',
      required: true,
      columnType: 'citext',
    },
    direcccion: {
      type: 'string',
      required: true,
      columnType: 'citext',
    },
    categoria: {
      type: 'string',
      required: true,
      columnType: 'citext',
    },
  },
};

