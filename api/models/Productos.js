/**
 * Productos.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    id: {
      type: 'string',
      columnName: 'codigo',
      required: true,
      columnType: 'citext',
    },
    descripcion: {
      type: 'string',
      required: true,
      columnType: 'citext',
    },
    valor: {
      type: 'number',
      required: true,
      columnType: 'citext',
    },
  },
};
