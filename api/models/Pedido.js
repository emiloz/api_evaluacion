/**
 * Pedido.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    nit: {
      model: 'clientes'
    },
    fecha: {
      type: 'string',
      allowNull: true,
    },
  },
};

